package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT ,?)";
	private static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT ,?)";
	private static final String SQL_FIND_ALL_USERS = "SELECT * FROM users";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SQL_DELETE_ALL_USERS = "DELETE FROM users WHERE login=?";
	private static final String SQL_INSERT_USER_TO_TEAM = "INSERT INTO users_teams VALUES (?, ?)";
	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login=?";
	private static final String SQL_FIND_TEAM_BY_NAME = "SELECT * FROM teams WHERE name=?";

	private static final String SQL_FIND_TEAMS_BY_USER_ID = "SELECT teams.id, teams.name " +
															"FROM users_teams AS ut " +
															"JOIN users ON ut.user_id = users.id " +
															"JOIN teams ON ut.team_id = teams.id " +
															"WHERE users.id = ?";

	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

	private static final String url = "jdbc:mysql://localhost:3306/";
	private static final String user = "root";
	private static final String pass = "root";

	private static DBManager instance;
	private static Connection connection;

	private static final Lock CONNECTION_LOCK = new ReentrantLock();


	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

	public static Connection getConnection() {
		if (connection != null) {
			return connection;
		}
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}


	public List<User> findAllUsers() throws DBException {
		Statement ps = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<>();
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().createStatement();
			rs = ps.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				User user = new User();
				users.add(user);
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
			}
		} catch (Exception e) {
			throw new DBException(e.getMessage(),e);
		} finally {
			close(rs);
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		PreparedStatement ps = null;
		ResultSet id = null;
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getLogin());
			if (ps.executeUpdate() != 1) {
				return false;
			}
			id = ps.getGeneratedKeys();
			if (id.next()) {
				int idField = id.getInt(1);
				user.setId(idField);
			}
		} catch (Exception e) {
			throw new DBException(e.getMessage(),e);
		} finally {
			close(id);
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		PreparedStatement ps = null;
		try {
			CONNECTION_LOCK.lock();
			connection.setAutoCommit(false);
			ps = getConnection().prepareStatement(SQL_DELETE_ALL_USERS);
			for(User user : users){
				ps.setString(1,user.getLogin());
				ps.addBatch();
			}
			ps.executeBatch();
			connection.commit();
			return true;
		} catch (SQLException ex) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(ps);
			setAutocommit();
			CONNECTION_LOCK.unlock();
		}
	}

	public User getUser(String login) throws DBException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try{
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_FIND_USER_BY_LOGIN);
			ps.setString(1, login);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(rs);
			close(ps);
			CONNECTION_LOCK.unlock();
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Team team = null;
		try{
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_FIND_TEAM_BY_NAME);
			ps.setString(1,name);
			rs = ps.executeQuery();
			if(rs.next()){
				team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(rs);
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Statement st = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try{
			CONNECTION_LOCK.lock();
			st = getConnection().createStatement();
			rs = st.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()){
				Team team = new Team();
				teams.add(team);
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
			}
		} catch (SQLException ex){
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(rs);
			close(st);
			CONNECTION_LOCK.unlock();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, team.getName());
			if (ps.executeUpdate() != 1) {
				return false;
			}
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				int idField = rs.getInt(1);
				team.setId(idField);
			}
		} catch (Exception ex) {
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(rs);
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		PreparedStatement ps = null;
		try {
			CONNECTION_LOCK.lock();
			connection.setAutoCommit(false);
			ps = getConnection().prepareStatement(SQL_INSERT_USER_TO_TEAM);
			for (Team team : teams) {
				ps.setInt(1, user.getId());
				ps.setInt(2, team.getId());
				ps.addBatch();
			}
			int[] usersGroups = ps.executeBatch();
			for (int i : usersGroups) {
				if (i != 1) {
					return false;
				}
			}
			connection.commit();
			return true;
		} catch (SQLException ex) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(ps);
			setAutocommit();
			CONNECTION_LOCK.unlock();
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_FIND_TEAMS_BY_USER_ID);
			ps.setInt(1, user.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				teams.add(team);
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
			}
		} catch (Exception ex) {
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(rs);
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		PreparedStatement ps = null;
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_DELETE_TEAM);
			ps.setString(1,team.getName());
			if (ps.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(),ex);
		}finally {
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		PreparedStatement ps = null;
		try {
			CONNECTION_LOCK.lock();
			ps = getConnection().prepareStatement(SQL_UPDATE_TEAM);

			ps.setString(1, team.getName());
			ps.setInt(2, team.getId());

			if (ps.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException ex) {
			throw new DBException(ex.getMessage(),ex);
		} finally {
			close(ps);
			CONNECTION_LOCK.unlock();
		}
		return true;
	}

	private static void close(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void setAutocommit() {
		try {
			getConnection().setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
